# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import os, sys
import requests
import hashlib
reload(sys)
sys.setdefaultencoding('utf8')

sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))
import lxml.html as lh
import config as config
from datetime import datetime
import time
import logging as logger


class Fetcher(object):
    def __init__(self):
        self.config = config
        self.logger = logger
        self.current_date = datetime.today().strftime('%Y-%m-%d')
        self.current_timestamp = datetime.now().strftime('%Y-%m-%d %H:%M:%S')

    def fetch_url_dom(self, url):
        dom_content = None
        response_code = None
        try:
            response = requests.get(url)
            response_code = response.status_code
            dom_content = response.text
        except Exception as ex:
            self.logger.exception("Exception occurred in getting URL Response for - {} and Trace {}".format(url, ex))
        return [dom_content, response_code]


class Classifier(object):
    def __init__(self):
        self.logger = logger
        self.config = config
        self.output_path = config.output_path

    def get_lxml_tree_dom(self, html):
        tree = None
        try:
            tree = lh.fromstring(html)
        except Exception as ex:
            self.logger.exception("Unable to parse given html to LXML - {}".format(ex))
        return tree

    def write_error_dom_file(self, url, html):
        current_timestamp = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        hash_object = hashlib.sha512(url)
        hash_hex = hash_object.hexdigest()
        # id = re.findall(r'[A-Z0-9]{10}', url)[0]
        output_file_name = self.config.output_path + hash_hex + "_" + str(current_timestamp) + "_" + "output.html"
        with open(output_file_name, "w") as fp:
            fp.write(html)

    def run_classification(self, domain, response_code, html, url=None):
        status = None
        try:
            tree = self.get_lxml_tree_dom(html)
            identifiers_ = tree.cssselect(config.selectors_map.get(domain))
            if identifiers_:
                status = "SUCCESS"
        except Exception as ex:
            self.logger.exception("Exception caught while running classification for the URL - {} and Trace {}".
                                  format(url, ex))
        if not status and response_code == 200:
            status = "CLASSIFICATION_ERROR"
            self.write_error_dom_file(url, html)
        elif not status and response_code != 200:
            status = "FETCH_ERROR"
            self.write_error_dom_file(url, html)
        return status


def main(domain_):
    file_ = open("test1.txt", "r")
    file1 = open("/tmp/output.txt", "a")  # append mode
    for url in file_:
        time.sleep(0.100)
        dom_content = Fetcher().fetch_url_dom(url)[0]
        resp_code = Fetcher().fetch_url_dom(url)[-1]
        classification_status = Classifier().run_classification(domain=domain_, response_code=resp_code,
                                                                html=dom_content, url=url)
        print("Response Code -{}".format(resp_code))
        print("classification_status is {} for url {}".format(classification_status, url))
        file1.write("URL: {} and Status: {} ".format(url, resp_code))
        file1.write("\n")
    file1.close()


if __name__ == '__main__':
    print("Inside Main Method")
    domain = sys.argv[1]
    sys.exit(main(domain))
