# -*- coding: utf-8 -*-
from fetch_stack import Classifier, Fetcher
from pybreaker import *
import config as config
from logger_util import LoggerUtil
from redis import Redis
from rq import Queue
import requests


class FetchJobsCircuitBreaker(object):

    def __init__(self):
        self.config = config
        self.logger = LoggerUtil().init_logging('/myntra/logs/',
                                                "circuit_breaker_log",
                                                "circuit_breaker.log")
        self.redis_conn = Redis(self.config.REDIS_HOST, "6379")
        self.q = Queue(connection=self.redis_conn)
        self.fetch_jobs = Queue("fetchJobs", connection=self.redis_conn)
        self.error_count = 0
        # self.db_breaker = CircuitBreaker(fail_max=10, reset_timeout=600)

    def get_fetch_jobs(self):
        return requests.get(self.config.FETCH_JOBS_POLLING_API)

    def detect_500(self, response):
        return response.status_code == 500

    def run_fetch_stack(self, url):
        # Your Logic goes here
        dom_content = Fetcher().fetch_url_dom(url)[0]
        resp_code = Fetcher().fetch_url_dom(url)[-1]
        classification_status = Classifier().run_classification(domain=domain_, response_code=resp_code,
                                                                html=dom_content, url=url)
        requests.post("")
        pass

    def main(self):
        fetch_jobs = self.get_fetch_jobs()
        for job in fetch_jobs:
            source = job.get("domain")
            url = job.get("url")
            db_breaker = CircuitBreaker(fail_max=10, reset_timeout=600, name=source, exclude=[lambda e: type(e) == requests.HTTPError and e.status_code < 500])
            state_storage = CircuitRedisStorage(STATE_CLOSED, self.redis_conn)
            example = db_breaker.call(self.run_fetch_stack, "pass_var")

    def on_success(self):
        self.error_count = max(0, self.error_count - 1)

    def on_error(self):
        self.error_count += 1
