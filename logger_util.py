import logging
import os
import sys
from logging import handlers
import settings
import gzip


class GZipRotator:
    def __call__(self, source, dest):
        os.rename(source, dest)
        f_in = open(dest, 'rb')
        f_out = gzip.open("%s.gz" % dest, 'wb')
        f_out.writelines(f_in)
        f_out.close()
        f_in.close()
        os.remove(dest)


class LoggerUtil:
    def __init__(self):
        pass


    @staticmethod
    def init_logging(dir_path, logger_name, logger_file_name, error_logger_file_name=''):
        # create logger
        """

        :rtype: object
        """
        logger = logging.getLogger(logger_name)

        if not len(logger.handlers):
            logger.setLevel(settings.LOG_LEVEL)

            if not os.path.exists(dir_path):
                os.makedirs(dir_path)

            formatter = logging.Formatter(
                '%(name)-15s %(asctime)-20s %(levelname)-8s %(filename)-15s %(lineno)-4d  %(funcName)15s() - %(message)s')
            # create file handler which logs even debug messages
            generic_info_logger = "processor_info.log"
            error_logger_file_name = "processor_error.log"
            fh1 = logging.FileHandler(dir_path + generic_info_logger)
            fh1.setLevel(settings.LOG_LEVEL)
            fh1.setFormatter(formatter)
            logger.addHandler(fh1)
            rh1 = logging.handlers.TimedRotatingFileHandler(filename=dir_path + generic_info_logger, when='M',
                                                            interval=30, backupCount=5)
            rh1.rotator = GZipRotator()

            logger.addHandler(rh1)

            # adding error logger for errors only
            if error_logger_file_name != '':
                fh2 = logging.FileHandler(dir_path + error_logger_file_name)
                fh2.setLevel(logging.ERROR)
                fh2.setFormatter(formatter)
                logger.addHandler(fh2)
                rh2 = logging.handlers.TimedRotatingFileHandler(filename=dir_path + error_logger_file_name, when='M',
                                                                interval=30, backupCount=5)
                rh2.rotator = GZipRotator()
                logger.addHandler(rh2)

        return logger

    @staticmethod
    def logger_with_stdout(logger_name):
        logger = logging.getLogger(logger_name)
        ch = logging.StreamHandler(sys.stdout)
        ch.setLevel(settings.LOG_LEVEL)
        formatter = logging.Formatter(
            '%(name)-15s %(asctime)-20s %(levelname)-8s %(filename)-15s %(lineno)-4d  %(funcName)15s() - %(message)s')
        ch.setFormatter(formatter)
        logger.addHandler(ch)
